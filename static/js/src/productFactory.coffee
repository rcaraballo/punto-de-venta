productFactory = ($resource) ->
  $resource '/api/products/'

angular
  .module 'pdv'
  .factory 'productFactory', productFactory