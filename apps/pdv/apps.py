from django.apps import AppConfig


class PdvConfig(AppConfig):
    name = 'apps.pdv'
