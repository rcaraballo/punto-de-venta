from django.db import models


class Products(models.Model):
    name = models.CharField(max_length=120)
    description = models.TextField(blank=True)
    unit_price = models.DecimalField(max_digits=8, decimal_places=2)

    def __str__(self):
        return self.name


class Inventory(models.Model):
    CHOICES_MEASUREMENT = (
        ('uni', 'Unidad(es)'), ('doc', 'Docena(s)'),
        ('lib', 'libra(s)'), ('gr', 'Gramo(s)'),
        ('kg', 'kg'), ('onz', 'Onz(s)'),
    )

    id_product = models.ForeignKey(Products)
    quantity = models.PositiveSmallIntegerField()
    unit_measurement = models.CharField(max_length=4, choices=CHOICES_MEASUREMENT)

    def __str__(self):
        return self.id_product.name
