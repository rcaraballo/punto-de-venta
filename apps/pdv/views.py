from django.shortcuts import render
from django.http import  HttpResponse
from django.views.decorators.csrf import csrf_exempt

from rest_framework import viewsets
from rest_framework.renderers import JSONRenderer
from  rest_framework.parsers import JSONParser

from .models import Products

from .serializers import ProductSerializer


class ProductViewSets(viewsets.ModelViewSet):
    queryset = Products.objects.all()
    serializer_class = ProductSerializer


def list_products(request):
    return render(request, 'pdv/templates/productController.html')