import os

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'puntoDeVenta.settings')
import django

django.setup()
from .models import Products, Inventory


def populate_database():
    products_list = [
          {
              "name": "Coca Cola 20onz.",
              "description": "Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500",
              "unit_price": 25

          }, {
              "name": "Pepsi 20onz.",
              "description": "Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500",
              "unit_price": 25
          }, {
              "name": "RedRock UVA 20onz.",
              "description": "Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500",
              "unit_price": 25
          }, {
              "name": "RedRock Merengue 20onz.",
              "description": "Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500",
              "unit_price": 25
          }, {
              "name": "RedRock Frambueza 20onz.",
              "description": "Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500",
              "unit_price": 25
          }, {
              "name": "7Up 20onz.",
              "description": "Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500",
              "unit_price": 25
          }
        ]

    for product in products_list:
        p = Products()
        p.name = product['name']
        p.description = product['description']
        p.unit_price = product['unit_price']
        p.save()

if __name__ == '__main__':
    print('Inicialize Population Script. . .')
    populate_database()