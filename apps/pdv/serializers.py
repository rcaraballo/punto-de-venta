from rest_framework import serializers
from .models import Products, Inventory


class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Products
        fields = ('id','name','description','unit_price',)
