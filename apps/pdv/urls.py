from django.conf.urls import url, include

from rest_framework import routers

from apps.pdv import views

router = routers.DefaultRouter()
router.register(r'products', views.ProductViewSets)

urlpatterns = [
    url(r'^api/', include(router.urls)),
    url(r'^home/$', views.list_products),
]