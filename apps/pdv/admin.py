from django.contrib import admin

from .models import Products, Inventory

@admin.register(Inventory)
class InventoryAdmin(admin.ModelAdmin):
    list_display = ('id_product', 'quantity', 'unit_measurement')
    ordering = ('id_product',)

@admin.register(Products)
class ProductAdmin(admin.ModelAdmin):
    list_display = ('name', 'unit_price')
    ordering = ('name',)